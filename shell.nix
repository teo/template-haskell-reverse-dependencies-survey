let
  pkgs = (builtins.getFlake "nixpkgs").legacyPackages.x86_64-linux;
in
  pkgs.mkShell {
    buildInputs = 
      [ (pkgs.python3.withPackages (ps: [ps.beautifulsoup4] ))
      ];
  }
