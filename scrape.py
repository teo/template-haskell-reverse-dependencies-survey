from bs4 import BeautifulSoup
import itertools

with open("th-reverse.html") as fp:
    soup = BeautifulSoup(fp, 'html.parser')

# second table in the doc
table = soup.find_all('table')[1]

for r in table.find_all('tr'):
  cols = r.find_all('td')
  if len(cols) <3:
      continue
  # 1 is package link, 2 is versioned link, 3 is rev deps
  #print(cols)
  name = cols[0].a.string
  version = cols[1].a.string
  link = "https://hackage.haskell.org" + cols[1].a.get('href')
  revdeps = next(cols[2].strings)[:-2]
  print(", ".join([name, version, link, revdeps]))
